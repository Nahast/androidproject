package com.epita.mti.velibproject;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.concurrent.atomic.AtomicInteger;

public class DetailsFragment extends Fragment{

    private Station station;
    private FrameLayout view;
    private Context ctx;

    private RelativeLayout layout;


    public DetailsFragment(){
        // Required empty public constructor
    }
    public static DetailsFragment newInstance(){

        return new DetailsFragment();
    }

    public void setStation(Station station, Context ctx) {
        this.station = station;
        this.ctx = ctx;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        view = (FrameLayout) inflater.inflate(R.layout.fragment_details, container, false);
        ctx = container.getContext();

        layout = new RelativeLayout(ctx);

        view.setBackgroundColor(Color.WHITE);

        view.addView(layout);

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(lp);

        initUI();

        return view;
    }

    private void initUI()
    {
        // TITLE
        TextView title = new TextView(ctx);
        title.setTextColor(Color.BLACK);
        title.setText(station.name);
        title.setTextSize(20);
        title.setId(generateViewId());
        title.setGravity(Gravity.CENTER);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        title.setLayoutParams(lp);

        layout.addView(title);

        // ADDRESS
        TextView address = new TextView(ctx);
        address.setTextColor(Color.BLACK);
        address.setText(station.address);
        address.setTextSize(20);
        address.setGravity(Gravity.CENTER);
        address.setId(generateViewId());

        RelativeLayout.LayoutParams lpa = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lpa.addRule(RelativeLayout.BELOW, title.getId());
        lpa.setMargins(0, 10, 0, 0);
        address.setLayoutParams(lpa);

        layout.addView(address);

        // COUNT
        TextView count = new TextView(ctx);
        count.setTextColor(Color.BLACK);
        count.setText("VELOS DISPONIBLES : " + station.bikes + "/" + station.stands);
        count.setTextSize(15);
        count.setGravity(Gravity.CENTER);
        count.setId(generateViewId());

        RelativeLayout.LayoutParams lpc = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lpc.addRule(RelativeLayout.BELOW, address.getId());
        lpc.setMargins(0, 10, 0, 0);
        count.setLayoutParams(lpc);

        layout.addView(count);
    }

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);
    public static int generateViewId() {
        for (;;) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

}
