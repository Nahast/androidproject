package com.epita.mti.velibproject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Valentin BARAT on 09/06/2017.
 * Please report any bug to valentin.barat@epita.fr
 */

public class StationHandler {

    private static StationHandler instance;
    public static StationHandler getInstance()
    {
        if (instance == null) instance = new StationHandler();
        return instance;
    }

    private ArrayList<Station> list;
    private StationHandler(){}

    public void loadList(JSONObject json)
    {
        try {
            JSONArray array = json.getJSONArray("records");

            list = new ArrayList<>();

            for (int i = 0; i < array.length(); i++) {
                list.add(new Station(array.getJSONObject(i).getJSONObject("fields")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Station> getList(){return list;}
}
