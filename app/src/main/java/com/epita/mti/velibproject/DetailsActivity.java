package com.epita.mti.velibproject;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import org.json.JSONException;

import java.util.ArrayList;

public class DetailsActivity extends Activity{

    private ArrayList<Station> stations;
    private Station thisStation;
    private int position;
    private FrameLayout view;


    private DetailsFragment[] fragments = new DetailsFragment[3];
    private Fragment current;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        view = (FrameLayout) findViewById(R.id.details_layout);
        stations = new ArrayList<>();

        final ArrayList<String> list = getIntent().getStringArrayListExtra("list");
        position = getIntent().getIntExtra("position", -1);
        stations = new ArrayList<>();


        try {
            for (String s : list) stations.add(new Station(s));
            thisStation = stations.get(position);
            initUI();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        createListener();
        initUI();
    }

    private void initUI()
    {
        loadFragments();
        changeFragment();

        ImageView img = new ImageView(this);
        img.setImageResource(R.mipmap.share_logo);

        FrameLayout.LayoutParams lpi = new FrameLayout.LayoutParams(100, 100);
        lpi.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        img.setLayoutParams(lpi);

        view.addView(img);

        img.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                Station current = stations.get(position);
                String message = "Station : " + current.name + "\nAdresse : " + current.address + "\nVélos libres : " + current.bikes + "/" + current.stands;

                Intent i=new Intent(android.content.Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(android.content.Intent.EXTRA_SUBJECT,"Partage de station");
                i.putExtra(android.content.Intent.EXTRA_TEXT, message);
                startActivity(Intent.createChooser(i,"Share via"));


            }
        });
    }

    private void loadFragments()
    {
        for(int i = 0; i < 3 ; i++)
        {
            fragments[i] = new DetailsFragment();
        }
        if (position > 0) {
            fragments[0].setStation(stations.get(position - 1), this);
        }
        fragments[1].setStation(stations.get(position)		, this);
        fragments[2].setStation(stations.get(position + 1)	, this);
        current = fragments[1];
    }

    private void changeFragment()
    {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.details_fragments, current);
        transaction.commit();
    }

    private void goToLeftFragment()
    {
        if(position == 0){return;}
        position--;

        fragments[2] = fragments[1];
        fragments[1] = fragments[0];

        current.getView().animate().translationXBy(1000).withEndAction(new Runnable(){
            @Override
            public void run(){
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.remove(current);
                transaction.commit();

                current = fragments[1];
                ((FrameLayout)findViewById(R.id.details_fragments)).removeAllViews();
                changeFragment();
            }
        }).setDuration(600).start();

        if(position == 0) { fragments[0] = null; return; }

        fragments[0] = new DetailsFragment();
        fragments[0].setStation(stations.get(position - 1), this);
    }

    private void goToRightFragment()
    {
        if(position == (stations.size() - 1)){ return; }
        position++;

        fragments[0] = fragments[1];
        fragments[1] = fragments[2];

        current.getView().animate().translationXBy(-1000).withEndAction(new Runnable(){
            @Override
            public void run(){
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.remove(current);
                transaction.commit();

                current = fragments[1];
                ((FrameLayout)findViewById(R.id.details_fragments)).removeAllViews();
                changeFragment();
            }
        }).setDuration(600).start();

        if(position == (stations.size() - 1)) { fragments[2] = null; return; }

        fragments[2] = new DetailsFragment();
        fragments[2].setStation(stations.get(position + 1), this);
    }

    private void createListener()
    {
        view.setOnTouchListener(new OnSwipeTouchListener(this) {

            public void onSwipeRight()
            {
                goToLeftFragment();
            }

            public void onSwipeLeft()
            {
                goToRightFragment();
            }
        });
    }
}
