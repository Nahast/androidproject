package com.epita.mti.velibproject;

import android.graphics.Point;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Valentin BARAT on 09/06/2017.
 * Please report any bug to valentin.barat@epita.fr
 */

public class Station{
    public enum Status { Open, Closed };

    public Status status;
    public String name, city, address, latitude, longitude, updateDate;
    public Integer number, stands, bikes, freeStands;
    public Boolean bonus;

    private JSONObject json;

    public Station(String str) throws JSONException{
        this(new JSONObject(str));
    }

    public Station(JSONObject json)
    {
        this.json = json;

        try {
            status = json.getString("status").equals("OPEN") ? Status.Open : Status.Closed;
            name = json.getString("name");
            city = json.getString("contract_name");
            bonus = json.getString("bonus").equals("True");
            address = json.getString("address");
            updateDate = json.getString("last_update");

            number = json.getInt("number");
            stands = json.getInt("bike_stands");
            bikes = json.getInt("available_bikes");
            freeStands = json.getInt("available_bike_stands");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString(){
        return this.json.toString();
    }
}
