package com.epita.mti.velibproject;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by nahast on 6/1/17.
 */

public class StationAdapter extends RecyclerView.Adapter<StationAdapter.ViewHolder> {

    private ArrayList<Station> mDataset;

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextView, mAdresse, mVelo;
        private Button mButton;
        private View v;

        public View getView() {
            return v;
        }

        public ViewHolder(View v) {
            super(v);
            this.v = v;
            mTextView = (TextView) v.findViewById(R.id.list_text_item);
            mAdresse = (TextView) v.findViewById(R.id.adresse);
            mVelo = (TextView) v.findViewById(R.id.velos);
            mButton = (Button) v.findViewById(R.id.go_btn);
        }

    }

    public StationAdapter(ArrayList<Station> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_line, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        View.OnClickListener click = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(holder.getView().getContext(), DetailsActivity.class);
                ArrayList<String> list = new ArrayList<>();

                for(Station s : mDataset) list.add(s.toString());

                i.putExtra("list", 		list);
                i.putExtra("position", 	position);
                holder.getView().getContext().startActivity(i);
            }
        };

        View.OnClickListener maps = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + Uri.encode(mDataset.get(position).address));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                holder.getView().getContext().startActivity(mapIntent);
            }
        };

        holder.getView().setOnClickListener(click);
        holder.mButton.setOnClickListener(click);

        holder.mAdresse.setOnClickListener(maps);

        holder.mTextView.setText(mDataset.get(position).name);
        holder.mAdresse.setText(mDataset.get(position).address);
        holder.mVelo.setText("" + mDataset.get(position).bikes + " / " + mDataset.get(position).stands);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}

